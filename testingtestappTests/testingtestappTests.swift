//
//  testingtestappTests.swift
//  testingtestappTests
//
//  Created by Emil Johansen on 15/12/2017.
//  Copyright © 2017 Emil Johansen. All rights reserved.
//

import XCTest
@testable import testingtestapp

class testingtestappTests: XCTestCase {
    
    var calculatorUnderTest: Calculator!
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        calculatorUnderTest = Calculator()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        calculatorUnderTest = nil
    }
    
    func testAddOne() {
        let x1: Double = 1
        XCTAssertEqual(calculatorUnderTest.addOne(x1), 2)
        
    }
    
    
}
