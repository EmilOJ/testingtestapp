//
//  Calculator.swift
//  testingtestapp
//
//  Created by Emil Johansen on 15/12/2017.
//  Copyright © 2017 Emil Johansen. All rights reserved.
//

import Foundation

class Calculator {
    var initValue: Double = 0
    
    func addOne(_ x: Double) -> Double {
        return x + 1
    }
}
