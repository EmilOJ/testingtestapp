//
//  ViewController.swift
//  testingtestapp
//
//  Created by Emil Johansen on 15/12/2017.
//  Copyright © 2017 Emil Johansen. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let calculator = Calculator()
        let myvar = calculator.addOne(1)
        print(myvar)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

